#!/bin/bash

USER_HOME=/home/node

if [ -z "$WEBROOT" ]; then
  WEBROOT="/app"
fi

# Disable Strict Host checking for non interactive git clones

mkdir -p -m 0700 $USER_HOME/.ssh
# Prevent config files from being filled to infinity by force of stop and restart the container 
echo "" > $USER_HOME/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n" >> $USER_HOME/.ssh/config

if [[ "$GIT_USE_SSH" == "1" ]] ; then
  echo -e "Host *\n\tUser ${GIT_USERNAME}\n\n" >> $USER_HOME/.ssh/config
fi

if [ ! -z "$SSH_KEY" ]; then
  echo $SSH_KEY > /root/.ssh/id_rsa.base64
  base64 -d $USER_HOME/.ssh/id_rsa.base64 > $USER_HOME/.ssh/id_rsa
  chmod 600 $USER_HOME/.ssh/id_rsa
fi

GIT_CONFIG_CMD="git config --global"

# Setup git variables
if [ ! -z "$GIT_EMAIL" ]; then
  $GIT_CONFIG_CMD user.email "$GIT_EMAIL"
fi
if [ ! -z "$GIT_NAME" ]; then
  $GIT_CONFIG_CMD user.name "$GIT_NAME"
  $GIT_CONFIG_CMD push.default simple
fi

if [ ! -z "$GIT_USERNAME" ] && [ ! -z "$GIT_PERSONAL_TOKEN" ]; then
  # Setup Gitlab Url with access token for root user
  $GIT_CONFIG_CMD --add url."https://${GIT_USERNAME}:${GIT_PERSONAL_TOKEN}@gitlab.com/".insteadOf "https://gitlab.com/"
  $GIT_CONFIG_CMD --add url."https://${GIT_USERNAME}:${GIT_PERSONAL_TOKEN}@gitlab.com/".insteadOf "git@gitlab.com:"
fi

# Dont pull code down if the .git folder exists
if [ ! -d "$WEBROOT/.git" ]; then
  # Pull down code from git for our site!
  if [ ! -z "$GIT_REPO" ]; then
    # Remove the test index file if you are pulling in a git repo
    if [ ! -z ${REMOVE_FILES} ] && [ ${REMOVE_FILES} == 0 ]; then
      echo "skiping removal of files"
    else
      rm -Rf $WEBROOT/*
    fi
    GIT_COMMAND='git clone --depth=1 -q '
    if [ ! -z "$GIT_BRANCH" ]; then
      GIT_COMMAND=${GIT_COMMAND}" -b ${GIT_BRANCH}"
    fi

    if [ -z "$GIT_USERNAME" ] && [ -z "$GIT_PERSONAL_TOKEN" ]; then
      GIT_COMMAND=${GIT_COMMAND}" ${GIT_REPO}"
    else
      if [[ "$GIT_USE_SSH" == "1" ]]; then
        GIT_COMMAND=${GIT_COMMAND}" ${GIT_REPO}"
      else
        GIT_COMMAND=${GIT_COMMAND}" https://${GIT_USERNAME}:${GIT_PERSONAL_TOKEN}@${GIT_REPO}"
      fi
    fi
    ${GIT_COMMAND} $WEBROOT || exit 1
    if [ ! -z "$GIT_TAG" ]; then
      git checkout ${GIT_TAG} || exit 1
    fi
    if [ ! -z "$GIT_COMMIT" ]; then
      git checkout ${GIT_COMMIT} || exit 1
    fi
  fi
fi

# Configure NPM
npm config set loglevel warn
npm config set progress false

if [ -z "$SKIP_NPM" ]; then
  # Try auto install for npm
  if [[ -f "$WEBROOT/package-lock.json" ]] && [[ -f "/usr/local/bin/npm" ]]; then
    npm install --quiet
    if [ "$APPLICATION_ENV" == "development" ]; then
      npm run --quiet --if-present dev
    else
      npm run --quiet --if-present prod
    fi
  fi
fi

# Run custom scripts
if [[ "$RUN_SCRIPTS" == "1" ]] ; then
  if [ -d "$WEBROOT/scripts/" ]; then
    # make scripts executable incase they aren't
    chmod -Rf 750 $WEBROOT/scripts/*; sync;
    # run scripts in number order
    for i in `ls $WEBROOT/scripts/`; do $WEBROOT/scripts/$i ; done
  else
    echo "Can't find script directory"
  fi
fi
