#!/bin/bash

if [ ! -z "$SERVER_ENTRY_POINT" ]; then
  if [ -f "$SERVER_ENTRY_POINT" ]; then
    sed -i "s#node server.js#node ${SERVER_ENTRY_POINT}#g" /etc/supervisord.conf
  else
    echo "Invalid entry point $SERVER_ENTRY_POINT. Exiting..."
    exit 1
  fi
fi

su-exec node '/setup.sh'

# Supervisor
if [ -z "$SKIP_CONF_SUPERVISOR" ]; then
  if [ -d "$WEBROOT/conf/supervisor/" ]; then
    mkdir -p /etc/supervisor/conf.d/
    cp $WEBROOT/conf/supervisor/* /etc/supervisor/conf.d/
    chown -Rf root.root /etc/supervisor/conf.d/*
  fi
fi

# Crontab
if [ -z "$SKIP_CONF_CRON" ]; then
  if [ -f "$WEBROOT/conf/crontab" ]; then
    cp $WEBROOT/conf/crontab /etc/crontabs/node
    chown -Rf root.root /etc/crontabs/node
  fi
fi

# Start supervisord and services
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
