FROM smujaddid/npm-git-aglio-drakov:latest

RUN apk -U upgrade --no-cache \
    && apk -U add --no-cache \
        supervisor \
    && mkdir -p /var/log/supervisor \
    && su-exec node npm i -g -q \
        aglio-theme-olio-printing-nav \
        apib2openapi

COPY conf/supervisord.conf /etc/supervisord.conf

COPY scripts/pull /usr/bin/pull
COPY scripts/push /usr/bin/push
COPY scripts/setup.sh /setup.sh
COPY scripts/start.sh /start.sh
RUN chmod 755 /usr/bin/pull \
    && chmod 755 /usr/bin/push \
    && chmod 755 /setup.sh \
    && chmod 755 /start.sh

ENV WEBROOT "/app"
RUN chown -Rf node.node $WEBROOT

CMD ["/start.sh"]
