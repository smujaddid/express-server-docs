#!/bin/bash

TAG="latest"

if [ ! -z "$1" ]; then
  TAG=$1
fi

docker build -t smujaddid/express-server-docs:$TAG .